import math
from sklearn import neighbors
import os
import os.path
import pickle
import datetime
from PIL import Image, ImageDraw
import face_recognition
from face_recognition.face_recognition_cli import image_files_in_folder
from flask import Flask, render_template, request, jsonify
from flaskext.mysql import MySQL

app = Flask(__name__)
mysql = MySQL()

app.config['MYSQL_DATABASE_USER'] = 'woraprat'
app.config['MYSQL_DATABASE_PASSWORD'] = '#Mindo0123'
app.config['MYSQL_DATABASE_DB'] = 'all-face'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}
X = []
y = []
names = []

def train(train_dir, model_save_path=None, n_neighbors=None, knn_algo='ball_tree', verbose=False):
    """
    :param model_save_path: (optional) path to save model on disk
    :param n_neighbors: (optional) number of neighbors to weigh in classification. Chosen automatically if not specified
    :param knn_algo: (optional) underlying data structure to support knn.default is ball_tree
    :param verbose: verbosity of training
    :return: returns knn classifier that was trained on the given data.
    """

    # Loop through each person in the training set
    for class_dir in os.listdir(train_dir):
        if not os.path.isdir(os.path.join(train_dir, class_dir)):
            continue

        print("class_dir path :" + class_dir)
        names.append(class_dir)

        # Loop through each training image for the current person
        for img_path in image_files_in_folder(os.path.join(train_dir, class_dir)):
            print("img_path :" + img_path)
            image = face_recognition.load_image_file(img_path)
            face_bounding_boxes = face_recognition.face_locations(image)

            if len(face_bounding_boxes) != 1:
                # If there are no people (or too many people) in a training image, skip the image.
                if verbose:
                    print("Image {} not suitable for training: {}".format(img_path, "Didn't find a face" if len(face_bounding_boxes) < 1 else "Found more than one face"))
            else:
                # Add face encoding for current image to the training set
                X.append(face_recognition.face_encodings(image, known_face_locations=face_bounding_boxes)[0])
                y.append(class_dir)

    # Determine how many neighbors to use for weighting in the KNN classifier
    if n_neighbors is None:
        n_neighbors = int(round(math.sqrt(len(X))))
        if verbose:
            print("Chose n_neighbors automatically:", n_neighbors)

    # Create and train the KNN classifier
    knn_clf = neighbors.KNeighborsClassifier(n_neighbors=n_neighbors, algorithm=knn_algo, weights='distance')
    knn_clf.fit(X, y)

    # Save the trained KNN classifier
    if model_save_path is not None:
        with open(model_save_path, 'wb') as f:
            pickle.dump(knn_clf, f)

    # print("show X :" + str(X))
    # print("show y :" + str(y))

    return knn_clf

def predict(X_img_path, knn_clf=None, model_path=None, distance_threshold=0.6):
    """
    Recognizes faces in given image using a trained KNN classifier

    :param X_img_path: path to image to be recognized
    :param knn_clf: (optional) a knn classifier object. if not specified, model_save_path must be specified.
    :param model_path: (optional) path to a pickled knn classifier. if not specified, model_save_path must be knn_clf.
    :param distance_threshold: (optional) distance threshold for face classification. the larger it is, the more chance
           of mis-classifying an unknown person as a known one.
    :return: a list of names and face locations for the recognized faces in the image: [(name, bounding box), ...].
        For faces of unrecognized persons, the name 'unknown' will be returned.
    """
    if not os.path.isfile(X_img_path) or os.path.splitext(X_img_path)[1][1:] not in ALLOWED_EXTENSIONS:
        raise Exception("Invalid image path: {}".format(X_img_path))

    if knn_clf is None and model_path is None:
        raise Exception("Must supply knn classifier either thourgh knn_clf or model_path")

    # Load a trained KNN model (if one was passed in)
    if knn_clf is None:
        with open(model_path, 'rb') as f:
            knn_clf = pickle.load(f)

    # Load image file and find face locations
    X_img = face_recognition.load_image_file(X_img_path)
    X_face_locations = face_recognition.face_locations(X_img)

    # If no faces are found in the image, return an empty result.
    if len(X_face_locations) == 0:
        return []

    # Find encodings for faces in the test iamge
    faces_encodings = face_recognition.face_encodings(X_img, known_face_locations=X_face_locations)

    # Use the KNN model to find the best matches for the test face
    closest_distances = knn_clf.kneighbors(faces_encodings, n_neighbors=1)
    are_matches = [closest_distances[0][i][0] <= distance_threshold for i in range(len(X_face_locations))]

    # Predict classes and remove classifications that aren't within the threshold
    return [(pred, loc) if rec else ("unknown", loc) for pred, loc, rec in zip(knn_clf.predict(faces_encodings), X_face_locations, are_matches)]

@app.route("/face-recognition/newface", methods=["GET", "POST"])
def newface():
    # try:
    #     name_index = y.index(name)
    # except ValueError:
    #     os.mkdir("dataset/" + name)
    if request.method == "POST":
        name = request.form.get("name")
        # file = request.files.getlist("file")
        file = request.files['file']

        print("name: " + name)
        print("filename: " + file.filename)
    
        target = os.path.join(APP_ROOT, "dataset/" + name)

        if not os.path.isdir(target):
            os.mkdir(target)
            
        file.save(os.path.join(target, file.filename))
        # sql connect
        conn = mysql.connect()
        cursor = conn.cursor() 

        # add face if name is exists
        try:
            # error if name not exists and go to except ValueError
            idex_name = y.index(name)

            load_img_from_file = face_recognition.load_image_file(file)
            img_face_bounding_boxes = face_recognition.face_locations(load_img_from_file)

            X.insert(idex_name, face_recognition.face_encodings(load_img_from_file, known_face_locations=img_face_bounding_boxes)[0])
            y.insert(idex_name, name)

            cursor.execute("INSERT INTO faces(name, face_path, time) VALUE(%s, %s)", (name, os.path.join(target, file.filename, datetime.datetime.now)))
            conn.commit()
        except ValueError:
            # add new face
            load_img_from_file = face_recognition.load_image_file(file)
            img_face_bounding_boxes = face_recognition.face_locations(load_img_from_file)
            X.append(face_recognition.face_encodings(load_img_from_file, known_face_locations=img_face_bounding_boxes)[0])
            y.append(name)

            cursor.execute("INSERT INTO faces(name, face_path) VALUE(%s, %s)", (name, os.path.join(target, file.filename)))
            conn.commit()
        except IndexError:
            return []

        knn_clf = neighbors.KNeighborsClassifier(n_neighbors=2, algorithm='ball_tree', weights='distance')
        knn_clf.fit(X, y)

        with open("trained_knn_model.clf", 'wb') as f:
            pickle.dump(knn_clf, f)

        print("X:" + str(len(X)) + " Y:" + str(len(y)))
        # print("X:" + str(X))
        print("y: "+ str(y))
        # X.append()
        # y.append(name)
        

        return jsonify(status = "success")

@app.route("/face-recognition/recogface", methods=["GET", "POST"])
def recogface():

    if request.method == "POST":
        file = request.files['file']
        target = os.path.join(APP_ROOT, "test/")

        if not os.path.isdir(target):
            os.mkdir(target)
        
        file.save(os.path.join(target, file.filename))

        predictions = predict(target + "/" + file.filename, model_path="trained_knn_model.clf")

        return jsonify(name = predictions[0][0])


@app.route("/")
def hello():
    # conn = mysql.connect()
    # cursor = conn.cursor()

    # cursor.execute("SELECT * FROM names")
    # data = cursor.fetchall()
    # res_name = []
    # if data:
    #     res_name = data['name']
    # for res in data:
    #     res_name.append(res[1])
    # print(list(data))

    for class_dir in os.listdir("dataset/"):
        if not os.path.isdir(os.path.join("dataset/", class_dir)):
            continue

        print("class_dir path :" + class_dir)
        names.append(class_dir)

    return jsonify(allname = names)

if __name__ == "__main__":
    # print("Training KNN classifier...")
    # classifier = train("dataset/", model_save_path="trained_knn_model.clf", n_neighbors=2)
    # print("Training complete!")
    # predictions = predict("test/jane3.jpg", model_path="trained_knn_model.clf")
    # print(predictions[0][0])
    # print(names)
    app.run(host="192.168.1.41", debug=True)
    
    